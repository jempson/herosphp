<?php
/**
 * 动态缓存配置信息
 * @author yangjian <yangjian102621@gmail.com>
 */
return array(

    'cache_dir'		=> APP_RUNTIME_PATH.'filecache/'.APP_NAME.'/', //缓存根目录

);